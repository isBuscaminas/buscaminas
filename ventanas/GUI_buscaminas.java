package ventanas;

import java.awt.BorderLayout;

import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import modelo.Buscaminas;
import modelo.Tablero;

import javax.imageio.ImageIO;

import javax.swing.ImageIcon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class GUI_buscaminas extends JFrame implements Observer {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// GUI_buscaminas frame = new GUI_buscaminas();
	// frame.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	/**
	 * Create the frame.
	 * 
	 * @throws IOException
	 */
	public GUI_buscaminas() {

		this.setVisible(true);
		int ancho, alto;

		// Cargamos ancho y alto del tablero
		ancho = Buscaminas.getBuscaminas().getTablero().getAncho();
		alto = Buscaminas.getBuscaminas().getTablero().getAlto();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Segun el tipo de buscaminas, le damos unas dimensiones a la interfaz
		if (ancho < 10) {
			setBounds(100, 100, 350, 400);
		} else if (ancho < 12) {
			setBounds(100, 100, 550, 600);
		} else {
			setBounds(100, 100, 650, 700);
		}

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JButton button = new JButton("New button");
		contentPane.add(button, BorderLayout.NORTH);
        button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				Buscaminas.getBuscaminas().empezarPartida();
				JFrame buscaminas = new GUI_buscaminas();
				buscaminas.setVisible(true);
			
				dispose();
			}
		});
		// JButton btnNewButton = new JButton("New button");
		// contentPane.add(btnNewButton, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(alto, ancho, 0, 0));
		contentPane.add(panel);

//		JButton btnNewButton_3 = new JButton("New button");
//		contentPane.add(btnNewButton_3, BorderLayout.SOUTH);

		for (int j = 0; j < alto; j++) {
			for (int i = 0; i < ancho; i++) {
				JButton btnNewButton = new JButton();
				// to remote the spacing between the image and button's borders
				btnNewButton.setMargin(new Insets(0, 0, 0, 0));
				panel.add(btnNewButton);
				JLabel etiqueta = new JLabel(i + "," + j);
				JLabel etiquetaOscura = new JLabel();
//				JLabel etiquetaClara = new JLabel();
//				panel.add(etiquetaOscura);
//				panel.add(etiquetaClara);
//				panel.add(etiqueta);
				etiqueta.setVisible(false);
				//etiquetaOscura.setVisible(false);
				//etiquetaClara.setVisible(false);
				btnNewButton.add(etiqueta);
				try {
					Image goscuro = ImageIO.read(getClass().getResource("../img/goscuro.png"));
					etiquetaOscura.setIcon(new ImageIcon(goscuro));
					btnNewButton.add(etiquetaOscura);
					etiquetaOscura.setVisible(true);
//					Image gclaro = ImageIO.read(getClass().getResource("../img/gclaro.png"));
//					etiquetaClara.setIcon(new ImageIcon(gclaro));
//					btnNewButton.add(etiquetaClara);
				} catch (IOException e1) {

					e1.printStackTrace();
				}
				//btnNewButton.setBackground(Color.gray);
				btnNewButton.setActionCommand(i + "," + j);
				btnNewButton.addMouseListener(new MouseListener() {

					@Override
					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void mousePressed(MouseEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void mouseExited(MouseEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void mouseEntered(MouseEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void mouseClicked(MouseEvent e) {
						if (e.getClickCount() == 1) {
							if (e.getButton() == MouseEvent.BUTTON1) {
								System.out.println("click izquierdo");
								e.getSource();
//								etiquetaOscura.setVisible(false);
//								etiquetaClara.setVisible(true);

								// Point = e.
								String cmd = etiqueta.getText();
								System.out.println(cmd);
								String splitXY = ",";
								String[] data = cmd.split(splitXY);
								// String cmd = e.getActionCommand();
								// String splitXY = ",";
								// String[] data = cmd.split(splitXY);
								Buscaminas.getBuscaminas().getTablero().descubrirCasilla(Integer.parseInt(data[0]),	Integer.parseInt(data[1]));

							}
							if (e.getButton() == MouseEvent.BUTTON3) {
								System.out.println("click derecho");
								String cmd = etiqueta.getText();
								System.out.println(cmd);
								String splitXY = ",";
								String[] data = cmd.split(splitXY);
								Buscaminas.getBuscaminas().getTablero().marcarDesmarcarCasilla(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
							}
						}
					}

				});
			}
		}
		Buscaminas.getBuscaminas().getTablero().addObserver(this); 
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		
		System.out.println("Funciona Observer");
		

	}
}
