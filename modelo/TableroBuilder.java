package modelo;


public abstract class TableroBuilder {

	private Tablero tablero;

	public TableroBuilder() {
	}

	public Tablero getTablero() {
		return tablero;
	}
	public void setTablero(Tablero tablero) {
		this.tablero = tablero;
	}
	public Tablero construirTablero() {
		tablero.colocarBombas();
		tablero.rellenarTablero();
		tablero.gestionarVacias();
		return tablero;	
	}

//	private void definirAtributos() {
//		setAncho();
//		setAlto();
//		setNumBombas();
//		System.out.println("Atributos definidos");
//	}

//	public abstract void setAncho();
//	public abstract void setAlto();
//	public abstract void setNumBombas();
}
