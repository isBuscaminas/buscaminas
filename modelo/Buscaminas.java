package modelo;

import java.util.Observable;

public class Buscaminas {
	private  int dificultad;
	private static Tablero unTablero;
	private static Buscaminas miBuscaminas = null;
	private Buscaminas() {

	}

	public static Buscaminas getBuscaminas() {

		if (miBuscaminas == null) {

			miBuscaminas = new Buscaminas();
		}
		return miBuscaminas;
	}

	public static void main(String[] args) {

	}
	
	public Tablero getTablero(){
		return unTablero;
	}
	public int getDificultad() {
		return dificultad;
	}
	public void setDificultad(int pDificultad) {
		this.dificultad = pDificultad;
	}
	
	public void empezarPartida () {
		DirectorTablero director = DirectorTablero.getDirectorTablero();
		
		unTablero = director.generarTablero(dificultad);	
	}
	
}
