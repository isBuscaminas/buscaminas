package modelo;

import java.util.Observable;
import java.util.Random;

import ventanas.GUI_buscaminas;

public class Tablero  extends Observable{

	private Casilla[][] tablero;
	private int ancho;
	private int alto;
	private int numBombas;
	private int numMarcadas;

	public Tablero(int pAncho, int pAlto, int pBombas) {
		this.ancho = pAncho;
		this.alto = pAlto;
		this.numBombas = pBombas;
		this.numMarcadas = 0;
		this.tablero = new Casilla[ancho][alto];
		// el tablero tendr� las dimensiones [0,ancho-1] x [0,alto-1]
		System.out.println("Tablero creado");

	}

	public int getAncho() {
		return ancho;
	}

	public Casilla[][] getTablero() {
		return this.tablero;
	}

	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	public int getAlto() {
		return alto;
	}

	public void setAlto(int alto) {
		this.alto = alto;
	}

	public int getNumBombas() {
		return numBombas;
	}

	public void setNumBombas(int numBombas) {
		this.numBombas = numBombas;
	}

	public boolean sinInicializar(int pAncho, int pAlto) {
		System.out.println("comprobando si no es bomba" + pAncho + "," + pAlto);
		return tablero[pAncho][pAlto] == null;
	}

	public void descubrirCasilla(int columna, int fila) {
		tablero[columna][fila].descubrirCasilla();
	}

	public void setCasilla(int rAncho, int rAlto, Casilla nuevaCas) {
		tablero[rAncho][rAlto] = nuevaCas;
	}

	public void colocarBombas() {

		int bombasRestantes = this.getNumBombas();
		while (bombasRestantes > 0) {
			Random randomAlto = new Random();
			int rAlto = randomAlto.nextInt(this.getAlto());
			Random randomAncho = new Random();
			int rAncho = randomAncho.nextInt(this.getAncho());
			if (this.sinInicializar(rAncho, rAlto)) {
				System.out.println("Check 2");
				Casilla nuevaCas = CasillaFactory.getCasillaFactory().createCasilla("CasillaBomba");
				this.setCasilla(rAncho, rAlto, nuevaCas);
				// Test
				nuevaCas.setX(rAncho);
				nuevaCas.setY(rAlto);
				
				bombasRestantes--;
				System.out.println("Bomba colocada en: " + rAncho + "," + rAlto);

			} else {
				System.out.println("La casilla "+ rAncho + ","+rAlto+" ya era una bomba" );
			}
		}
		System.out.println("Bombas colocadas");
	}


	public Integer numBombasAlrededor(int col, int fila) {
		int bombas = 0;
		for (int c = col - 1; c <= col + 1; c++) {
			if (c >= 0 && c < this.getAncho()) {
				for (int f = fila - 1; f <= fila + 1; f++) {
					if (f >= 0 && f < this.getAlto()) {
						if (c != col || f != fila) {
							if (this.tablero[c][f] != null) {
								if (this.tablero[c][f] instanceof CasillaBomba) {
									bombas++;
								}
							}
						}
					}
				}
			}
		}
		return bombas;
	}

	public void rellenarTablero() {
		int bombas = 0;
		int j;
		for (int i = 0; i < this.getAncho(); i++) {
			for (j = 0; j < this.getAlto(); j++) {
				if (tablero[i][j] == null) {
					bombas = numBombasAlrededor(i, j);
					if (bombas > 0) {
						tablero[i][j] = CasillaFactory.getCasillaFactory().createCasilla("CasillaNumero");
						((CasillaNumero) tablero[i][j]).setNumBombas(bombas);
						System.out.println("Hemos creado una casilla numero" + bombas);
						tablero[i][j].setX(i);
						tablero[i][j].setY(j);
					} else {
						tablero[i][j] = CasillaFactory.getCasillaFactory().createCasilla("CasillaVacia");
						System.out.println("Hemos creado una casilla vacia");
						tablero[i][j].setX(i);
						tablero[i][j].setY(j);
					}
				}
			}
		}
	}

	public void gestionarVacias() {
		for (int i = 0; i < this.getAncho(); i++) {
			for (int j = 0; j < this.getAlto(); j++) {
				if (tablero[i][j] instanceof CasillaVacia)
					this.rellenarVecinos(i, j);
			}
		}
	}

	public void rellenarVecinos(int col, int fila) {
		int cont = 0;
		for (int c = col - 1; c <= col + 1; c++) {
			if (c >= 0 && c < this.getAncho()) {
				for (int f = fila - 1; f <= fila + 1; f++) {
					if (f >= 0 && f < this.getAlto()) {
						if (c != col || f != fila) {
							System.out.println("Add vecino");
							((CasillaVacia) tablero[col][fila]).getListaVecinos().anadirVecino(tablero[c][f]);
							cont++;
						}
					}
				}
			}
		}
		System.out.println("Numero de vecinos de " + col + " " + fila + " es " + cont);
	}

	public void marcarDesmarcarCasilla(int columna, int fila) {
		boolean marcar;
				
		marcar = tablero[columna][fila].marcarDesmarcarCasilla();
		if (marcar){
			numMarcadas++;
		} else {
			numMarcadas--;
		}
		setChanged();
		notifyObservers(tablero);
	}
}
