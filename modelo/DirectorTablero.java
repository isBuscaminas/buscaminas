package modelo;

public class DirectorTablero { // implements observer

	static DirectorTablero miDirectorTablero;
	TableroBuilder tableroBuilder;

	private DirectorTablero() {

	}

	public static DirectorTablero getDirectorTablero() {

		if (miDirectorTablero == null) {
			miDirectorTablero = new DirectorTablero();
		}
		return miDirectorTablero;
	}

//	public Tablero getTablero(int pDificultad) {
//		return generarTablero(pDificultad);
//		//return tableroBuilder.getTablero();
//	}

	public void setTableroBuilder(TableroBuilder tb) {
		tableroBuilder = tb;
	}

	public Tablero generarTablero(int dificultad) {
   ;
		if (dificultad == 1) {
			
			this.setTableroBuilder(new TableroFacil());
			
		} else if (dificultad == 2) {
			
			this.setTableroBuilder(new TableroNormal());
		} else if (dificultad == 3) {
			
			this.setTableroBuilder(new TableroDificil());
		}
		return tableroBuilder.construirTablero();
		
	}
}
