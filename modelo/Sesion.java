package modelo;

import java.util.Observable;

import javax.swing.JFrame;

import ventanas.GUI_buscaminas;

public class Sesion extends Observable{
	 
	private static Sesion mSesion;
	private Jugador jugador;
	
	public Jugador getJugador() {
		return jugador;
	}

	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}

	private Sesion() {
	//la dificultad le va al Buscaminas, que va al director que dice que tipo de tablero quiere
		//encapsular todo en el propio buscaminas, que buscaminas sea SesionBuscaminas, Buscaminas tendra un atributo jugador
		//Crear clase jugador con string y puntuacion.
	
	}

 //	public static void main(String[] args) {
//		iniciarPartida(1);
//	}
	
	
	public static Sesion getSesion() {
		if (mSesion == null){
			mSesion = new Sesion();
		}
		return mSesion;
	}
	
	public  void iniciarPartida(){
		setChanged();
		notifyObservers();
	//	Buscaminas.getBuscaminas().setDificultad(1);
//	Buscaminas.getBuscaminas().empezarPartida();
//	JFrame buscaminas = new GUI_buscaminas();
	}
	
//	public void reiniciarPartida(){
//		
//		Buscaminas.getBuscaminas().empezarPartida();
//		JFrame buscaminas = new GUI_buscaminas();
//		buscaminas.setVisible(true);
//	
//		
//	}
}
